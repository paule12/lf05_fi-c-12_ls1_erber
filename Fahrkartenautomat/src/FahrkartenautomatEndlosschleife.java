import java.util.Scanner;

public class FahrkartenautomatEndlosschleife{

	
	public static void main (String[] args)
	{
	
	while (true) {
		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
		double rueckgabebetrag = fahrkartenBezahlen (zuZahlenderBetrag);
		fahrkartenAusgeben();
		r�ckgeldAusgeben(rueckgabebetrag);
		
		System.out.println("\nVergessen sie bitte nicht, die Fahrscheine\n"+
				            "vor Fahrtantritt entwerten zu lassen! \n");

	}
	}

	public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
	
	 double ticketEinzelpreis = 1.0;
     boolean running = false;
	
     do { 
    	 System.out.print("Bitte w�hlen sie eine der folgenden Ticketarten:\n\nEinzelfahrschein Regeltarif AB [2,90 EUR] (1)\r\n"
    	 		+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\r\n"
    	 		+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\r\n");
     int auswahl = tastatur.nextInt();
     
     
     switch (auswahl) {
     case 1: ticketEinzelpreis = 2.9; running = false; break;
     case 2: ticketEinzelpreis = 8.6; running = false; break;
     case 3: ticketEinzelpreis = 23.5; running = false; break;
     default:
    	 System.out.println("\nGeben sie eine Ganzezahl zwischen 1und 3 ein.\n");
    	 
    	 running = true;
    	 break;
    	 
     }
     } while (running); 
     
     System.out.print("Wie viele Tickets werden sie kaufen?:  ");
     double anzahlTickets = tastatur.nextInt();
     
     return ticketEinzelpreis * anzahlTickets;
     
	}
	
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag)  {
		Scanner tastatur = new Scanner(System.in);
		
		
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneM�nze;
		 
		
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
		{
			System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),"  ");
			System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 EURO): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
	
	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	
	}
	
	public static void warte (int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrsscheine werden Ausgegeben.");
		for (int i = 0; i < 8; i++)
			
		{
			System.out.print("=");
		     warte(250);
		}
	System.out.println("\n\n");
	
	}
	
	public static void m�nzeAusgeben(double r�ckgabebetrag, String einheit) {
	
		while(r�ckgabebetrag >= 2.0) // 2 EURO
        {
     	  System.out.println("2 " + einheit);
	          r�ckgabebetrag -= 2.0;
        }
        while(r�ckgabebetrag >= 1.0) // 1 EURO
        {
     	  System.out.println("1" + einheit);
	          r�ckgabebetrag -= 1.0;
        }
        while(r�ckgabebetrag >= 0.50) // 50 CENT
        {
     	  System.out.println("50" + einheit);
	          r�ckgabebetrag -= 0.50;
        }
        while(r�ckgabebetrag >= 0.20) // 20 CENT
        {
     	  System.out.println("20" + einheit);
	          r�ckgabebetrag -= 0.20;
        }
        while(r�ckgabebetrag >= 0.10) // 10 CENT
        {
     	  System.out.println("10" + einheit);
	          r�ckgabebetrag -= 0.10;
        }
        while(r�ckgabebetrag >= 0.05)// 5 CENT
        {
     	  System.out.println("5" + einheit);
	          r�ckgabebetrag -= 0.05;
        }

	}
	
	public static void r�ckgeldAusgeben(double r�ckgabebetrag) {
    	if(r�ckgabebetrag > 0.0)

    	{ 
    		 
    		System.out.printf("Der R�ckgabebetrag in H�he von %. 2f EURO", r�ckgabebetrag);
    		System.out.println("\nwird in folgenden m�nzen ausgezahlt: ");
    		m�nzeAusgeben(r�ckgabebetrag, " EURO");
    	}
    		
    	}



}




	
	
	
	
	
	
	
	
