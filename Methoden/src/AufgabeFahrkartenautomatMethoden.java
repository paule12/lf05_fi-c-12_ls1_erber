import java.util.Scanner;

public class AufgabeFahrkartenautomatMethoden {
	public static void main(String[] args) {
	
		Scanner tastatur = new Scanner (System.in);
		
		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;
		double r�ckgabebetrag;
		double einzelpreis;
		short anzahltickets;
		
		System.out.println("wie viel kostet ein Ticket?");
		einzelpreis = tastatur.nextDouble(); 
		
		System.out.println("Wie viele Tickets m�chten sie Kaufen ?");
		anzahltickets = tastatur.nextShort();
		
		zuZahlenderBetrag = einzelpreis * anzahltickets;
		
		//Geld wird eingeworfen
		
		eingezahlterGesamtbetrag = 0.00;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag);
		
		{ 
			System.out.printf(" Noch zu zahlen: %.2f", + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print(" Euro \nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
			 eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		
		// Die Fahrkarten werden Ausgegeben
		
		 System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	    	   System.out.print("=");
	    	   try { 
	    		   Thread.sleep(250);
	    	   } catch (InterruptedException e) {
	   			// TODO Auto-generated catch block
	   			e.printStackTrace();
	    	   }
	       }
		System.out.println("\n\n");
		
		//R�ckgeld wird berechnet und ausgegeben 
		
		r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if(r�ckgabebetrag > 0.00)
		{
			System.out.printf("R�ckgabebetrag in einer h�he von %.2f ", r�ckgabebetrag);
			System.out.println("� wird in den Folgenden M�nzen ausgegeben:");
			
			while(r�ckgabebetrag >= 2.00)  // Zwei Euro
			{
				System.out.println("2.00�");
				r�ckgabebetrag -= 2.00;
			}
			while(r�ckgabebetrag >= 1.00)     // Ein Euro
		    {
				System.out.println("1.00�");
				r�ckgabebetrag -= 1.00;
		    }
			while(r�ckgabebetrag >= 0.50);    // F�nfzig Cent 
			{
				System.out.println("0.50�");
				r�ckgabebetrag -= 0.50;
			}
			while(r�ckgabebetrag >= 0.20);	  // Zwanzig Cent
			{
				System.out.println("0.20�");
				r�ckgabebetrag -= 0.20;
			}
			while(r�ckgabebetrag >= 0.10);	  // Zehn cent
			{
				System.out.println("0.10�");
				r�ckgabebetrag -= 0.20;
			}
			while(r�ckgabebetrag >= 0.05);    // F�nf Cent
			{
				System.out.println("0.05�");
				r�ckgabebetrag -= 0.05;
			}
		}
	
	System.out.println("\nVergessen sie bitte nicht, die Fahrscheine\n"+
						"vor Fahrtantritt entwerten zu lassen!\n"+ 
						" Wir w�nschen ihnen eine angenemhe Fahrt.");
	
	
	}}


		// Ich habe mich f�r mein Teil f�r Short entschieden, da es aus meinen Augen f�r die Anzahl der Tickets ausreichen wird.
